﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

	private uiManager uimanager;

	public GameObject pauseMenucanvas;

	public bool SoundToggle = false;
	public bool PmActive = false;

	public Text Toggletext;
	
	void Start(){

		uimanager = GameObject.Find ("UIManager").GetComponent<uiManager> ();

	}

	// Update is called once per frame
	void Update () {
	
		if (uimanager.paused) {

			pauseMenucanvas.SetActive (true);
			PmActive = true;
		} 
		else {
			pauseMenucanvas.SetActive(false);
			PmActive = false;
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			uimanager.paused = !uimanager.paused;
		}
		if (SoundToggle && PmActive) {

			//TÄSSÄ PELIN ÄÄNET POIS
			Toggletext.text = "Sounds are disabled";

		}
		if (!SoundToggle && PmActive) {
			//ÄÄNET TAKAS PÄÄLLE
			Toggletext.text = "Sounds are enabled";
		}

	}

	public void Resume(){

		uimanager.paused = false;
		Time.timeScale = 1;
	}

	public void LevelSelect(){

		Application.LoadLevel ("LevelSelect");
	}

	public void Quit(){

		Application.LoadLevel ("MainMenu");
	}

	public void SoundToggleClicked(){

		SoundToggle = !SoundToggle;

	}
}
