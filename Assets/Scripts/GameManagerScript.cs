﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;


public class GameManagerScript : MonoBehaviour {


    //screen size
    public static float screenHeight;
    public static float screenWidth;

    //kuolemien ja kolikkojen kokonaismäärä, turhaan täällä jos Player.pref toimii
    public int totalDeaths;
    public int totalCoins;

    public GameObject StarCoinPrefab;
    public GameObject StarParticlePrefab;
        
    public GameObject BackgroundFar;
    public GameObject BackgroundMid;
    public GameObject BackgroundClose;
    
    
    private static GameManagerScript m_Instance = null;
    
    public static GameManagerScript Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = (GameManagerScript)FindObjectOfType(typeof(GameManagerScript));
            }

            return m_Instance;
        }
    }

    void Awake()
    {
        //otetaan näytön resoluutio talteen
        screenHeight = Screen.height;
        screenWidth = Screen.width;
	}


    void Start()
    {

    }


    void Update()
    {
        
    }
}
