﻿using UnityEngine;
using System.Collections;

public class UpperDamage : MonoBehaviour {


    public PlayerScript playerScript;
    public bool headCanTakeDamage = false;
    public bool gravityButtonPressed_2 = false;

	
	void Start () 
    {
        playerScript = GetComponentInParent<PlayerScript>();
	}
	
	
	void Update () 
    {        
        if (gravityButtonPressed_2)
        {
            //viive ettei pää osu lattiaan
            StartCoroutine(WaitAfterGravityChange());
            gravityButtonPressed_2 = false;
        }

        //pää collider voi taas osua kun ollaan tehty gravity flip ja koskettu maahan
        else if (playerScript.grounded)
        {
            headCanTakeDamage = false; 
        }
	}

    //täytyy saada pieni viive ettei pää osu lattiian painovoiman vaihdossa
    IEnumerator WaitAfterGravityChange()
    {        
        float timeToWait = 0.05f;
        yield return new WaitForSeconds(timeToWait);
        headCanTakeDamage = true;
    }
    

    void OnTriggerEnter2D(Collider2D col)
    {
        //näihin kuolee
        if (col.tag == "Platform" && headCanTakeDamage)
        {
            playerScript.damageTaken = true;
            playerScript.damageSquashed = true;
        }

        //näihin EI kuole
        if (col.tag == "Teleportti" || col.tag == "TeleporttiOut")
        {
            playerScript.damageTaken = false;
        }
    }
}
