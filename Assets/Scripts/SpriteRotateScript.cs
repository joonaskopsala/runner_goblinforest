﻿using UnityEngine;
using System.Collections;

public class SpriteRotateScript : MonoBehaviour 
{
    //pyörimisnopeutta varten
    public PlayerScript playerScript;

    //MaaliScript jotta huomataan milloin pelaaja on maalissa
    public MaaliScript maaliScript;
    
    //PlayerScript säätää nämä
    public bool JumpPressedForAnim;
    public bool JumpHighForAnim;

    //animaatiot
    private Animator anim;

    //pyörimistä varten
    public float rotationSpeed;
    public Quaternion defaultRotation;
    public bool canRotate = false;
    public bool canRotateInAir = false;


	void Start () 
    {
        playerScript = GetComponentInParent<PlayerScript>();

        maaliScript = GameObject.FindGameObjectWithTag("Maali").GetComponent<MaaliScript>();

        anim = GetComponent<Animator>();

        defaultRotation = Quaternion.Euler(0, 0, 0);
	}
	

	void Update () 
    {
        //asetetaan yhteydet animaatioiden muuttujiin
        anim.SetBool("Grounded", playerScript.grounded);
        anim.SetBool("JumpButtonPressed", JumpPressedForAnim);
        anim.SetBool("DamageTaken", playerScript.damageTaken);
        anim.SetBool("Moving", playerScript.levelStarted);
        anim.SetBool("Squashed", playerScript.damageSquashed);
        anim.SetBool("SpinJump", JumpHighForAnim);

        //keinotekoinen pyörimisvauhti liikkeen nopeuden mukaan
        rotationSpeed = playerScript.rb2D.velocity.x * -0.75f;

        //nykyinen kulma muuttujaan
        float playerAngleNow = Mathf.Abs(transform.rotation.z);
        
        //jos ilmassa pelaaja voi pyöriä
        if (!playerScript.grounded && canRotateInAir)
        {
            canRotate = true;
        }
        
        //kun maassa lopetetaan pyöriminen kun pelaaja on lähellä 0 kulmaa
        if ( playerScript.grounded &&
            (playerAngleNow <= 0.15f &&
             playerAngleNow >= 0f))
        {
            canRotate = false;
            canRotateInAir = true;
        }

        //jos pelaaja on maalissa pyöriminen pitää lopettaa
        if (maaliScript.voittoMark)
        {
            rotationSpeed = 0f;
        }

        //itse pyörimisen toteutus
        if (canRotate)
        {
            transform.rotation = Quaternion.AngleAxis(rotationSpeed, Vector3.forward) * transform.rotation;
        }
        else
        {
            transform.rotation = defaultRotation;
        }

        //kuoleman sattuessa resetoidaan pelaajan kulma, jotta animaatiot sattuvat kohdilleen
        if (playerScript.damageTaken)
        {
            transform.rotation = defaultRotation;
        }

        //korkeassa hypyssä suoristetaan pelaaja jotta nähdään pyörimisanimaatio
        if (JumpHighForAnim)
        {
            transform.rotation = defaultRotation;

            canRotate = false;
            canRotateInAir = false;
        }

        




        //nollataan animaatiomuuttujia jotka suoritetaan vain kerran
        JumpHighForAnim = false;
	}
}
