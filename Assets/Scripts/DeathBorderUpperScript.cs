﻿using UnityEngine;
using System.Collections;

public class DeathBorderUpperScript : MonoBehaviour {


    private PlayerScript playerScript;


    void Start()
    {
        playerScript = GameObject.FindGameObjectWithTag("Player").
                       GetComponent<PlayerScript>();
    }

    //pelaaja lentää kaukana taustalla kun osuu ylälaitaan
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            //tässä vihreänpallon (pelaaja) tulee lentää alaspäin horisontissa


            //pelaaja ottaa damagea vasta kun on lentänyt horisointissa
            playerScript.damageTaken = true;
        }
    }
}
