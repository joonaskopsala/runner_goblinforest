﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

    
    private PlayerScript playerScript; 
	public TeleportScript teleportScript;

    //pietään kamera pelaajan ylä ja alapuolella
    public float xLimit = 14f;
    public float yLimit = 7f;
    
    public Vector3 minCameraPos;
    public Vector3 maxCameraPos;
    
    //kamera Lerppiä varten
    public bool cameraIsLerping = false;
	public bool cameraTeleportLerp = false;
	public bool voimaBoolean = false;
    public Vector3 playerDeathPosition;
    public Vector3 playerStartPosition;  
	public Vector3 playerTeleStart;
	public Vector3 playerTeleStop;
    public float timeLerpingStarted;
    
	
	void Start () 
    {
        playerScript = GameObject.FindGameObjectWithTag("Player").
                        GetComponent<PlayerScript>();
        teleportScript = GameObject.FindGameObjectWithTag("Teleport").
                        GetComponent<TeleportScript>();
	}
	
	
	void Update () 
    {
		//TÄSSÄ TELEPORTTIKAMERAHOMMIA
		if (playerScript.telePorting)
		{
			playerScript.telePorting = false;
			cameraTeleportLerp = true;
			
			//ei tarvi lisätä Z-akselin -10f kameralle kun tämä on kameran koordinaatti			
            playerTeleStart = teleportScript.teleportEnter;
            
            //pelaajan Y-akseliin pitää lisätä 4 ettei pelaaja ole ruudun keskellä aloittaessa
            playerTeleStop = teleportScript.teleportExit + new Vector3(xLimit, yLimit, -10f);

			timeLerpingStarted = Time.time;
		}
		
		//kamera-ajo elikkä Lerppaus
		if(cameraTeleportLerp)
		{  
			//lerpin tarvitsema %-arvo lasketaan etukäteen eikä ite lerpin sisällä
			float timeSinceStartedLerping = Time.time - timeLerpingStarted;
			float percentanceComplete = timeSinceStartedLerping / 1f;
			
			//itse lerppaus
			transform.position = Vector3.Lerp(playerTeleStart,
			                                  playerTeleStop,
			                                  percentanceComplete );
			
			//lerppauksen lopetus kun sekunti on kulunut 
			if (percentanceComplete >= 1f )
			{
				cameraTeleportLerp = false;
				voimaBoolean = true;
			}
		}



		//NORMAALIKAMERAHOMMIA
        //kun pelaaja on kuollut otetaan alkuarvot kamera-ajoon
        if (playerScript.damageTaken && playerScript.cameraCanLerp)
        {
            cameraIsLerping = true;

            //kameran sijainti silloin kun pelaaja on ottanut damagea
            playerDeathPosition = playerScript.deathPosition;

            //pelaajan Y-akseliin pitää lisätä 4 ettei pelaaja ole ruudun keskellä aloittaessa
            //lisätään kameran tarvitsema Z-akselin -10f pelaajasta katsottuna
            playerStartPosition = playerScript.startPosition + new Vector3(xLimit, yLimit, -10f);

            timeLerpingStarted = Time.time;
        }

        //kamera-ajo elikkä Lerppaus jos pelaaja on kuollut ja arvot tallessa
        if(cameraIsLerping)
        {   
            //lerpin tarvitsema %-arvo lasketaan etukäteen eikä ite lerpin sisällä
            float timeSinceStartedLerping = Time.time - timeLerpingStarted;
            float percentanceComplete = timeSinceStartedLerping / 1f;

            //itse lerppaus
            transform.position = Vector3.Lerp(playerDeathPosition,
                                              playerStartPosition,
                                              percentanceComplete );

            //lerppauksen lopetus kun sekunti on kulunut 
            if (percentanceComplete >= 1f )
            {
                cameraIsLerping = false;
                playerScript.cameraCanLerp = false;
            }
        }
        

        //normaalitilanne, pidetään pelaaja kameran sisällä
        if (!cameraIsLerping && !cameraTeleportLerp)
        {
            minCameraPos = new Vector3(
                (playerScript.transform.position.x + xLimit),
                (playerScript.transform.position.y - yLimit),
                (transform.position.z));

            maxCameraPos = new Vector3(
                (playerScript.transform.position.x + xLimit),
                (playerScript.transform.position.y + yLimit),
                (transform.position.z));

            transform.position = new Vector3(
                   Mathf.Clamp(transform.position.x, minCameraPos.x, maxCameraPos.x),
                   Mathf.Clamp(transform.position.y, minCameraPos.y, maxCameraPos.y),
                   Mathf.Clamp(transform.position.z, minCameraPos.z, maxCameraPos.z));
        }
	}
}
