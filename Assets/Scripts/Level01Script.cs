﻿using UnityEngine;
using System.Collections;

public class Level01Script : MonoBehaviour {

    //taustakuvan muuttuja
    public SpriteRenderer backgroundImage;
    //aseta haluttu taustakuva inspectorissa
    public Sprite newImage;
        
    //tämä määrää mitkä tähtikolikot luodaan leveliin
    public bool[] StarCoinsInThisLevel = new bool[3];

    //starcoinien positionit levelissä
    public Vector3[] StarCoinPositions = new Vector3[3];
    
    //tähän taulukkoon tähtikolikot
    public GameObject[] StarCoins = new GameObject[3];

    //StarCoinScripti kolikkojen positioneiden tarkastusta varten 
    public StarCoinScript[] starCoinScript = new StarCoinScript[3];
    public GameObject[] StarCoinScriptsInThisLevel = new GameObject[3];
 

   
    void Awake()
    {
        GetUncollectedStarCoins();

        SetStarCoinPositions();

        SpawnStarCoins();        
    }


	void Start () 
    {
        //haetaan backgroundin SpriteRenderer
        backgroundImage = GameObject.FindGameObjectWithTag("Background").
                            GetComponent<SpriteRenderer>();
        //asetetaan inspectorissa määritelty kuva taustaksi
        backgroundImage.sprite = newImage;

        /*
        //haetaan starCoinScriptit StarCoineista
        if (StarCoinScriptsInThisLevel == null)
        {
            StarCoinScriptsInThisLevel = GameObject.FindGameObjectsWithTag("StarCoin");
        }
        print(StarCoinScriptsInThisLevel.Length);
        for (int i = 0; i < 3; i++)
        {
            starCoinScript[i] = StarCoinScriptsInThisLevel[i].GetComponent<StarCoinScript>();
        }*/
        //starCoinScript = GameObject.FindGameObjectsWithTag("StarCoin");//.GetComponent<StarCoinScript>();
    }

    
    void Update() 
    {
        //CheckCollectedCoins();

        //print(StarCoinsInThisLevel[0] + " " + StarCoinsInThisLevel[1] + " " + StarCoinsInThisLevel[2]);
	}


    void GetUncollectedStarCoins()
    {
        //haetaan tähtikolikkojen määrä leveliin jos joitain on jo kerätty

        //tämä for vain testi käyttöä varten, oikeasti kolikot haetaan PlayerPrefX:stä
        for (int i = 0; i < 3; i++)
        {
            StarCoinsInThisLevel[i] = true;
        }
    }


    void SetStarCoinPositions()
    {
        //KIRJOITAITSE! kolikkojen positionit scenessä kirjoitetaan itse tähän
        StarCoinPositions[0] = new Vector3(20f, 5f, 0f);
        StarCoinPositions[1] = new Vector3(25f, 5f, 0f);
        StarCoinPositions[2] = new Vector3(272f, 2.35f, 0f);
    }


    void SpawnStarCoins()
    {
        //luodaan tarvittavat tähtikolikot
        for (int i = 0; i < 3; i++)
        {
            if (StarCoinsInThisLevel[i])
            {
                StarCoins[i] = Instantiate(GameManagerScript.Instance.StarCoinPrefab,
                                            StarCoinPositions[i],
                                            Quaternion.Euler(0, 0, 0)) as GameObject;
            }
        }
    }

    /*
    void CheckCollectedCoins()
    {
        for( int k = 0; k < 3; k++)
        {
            if (starCoinScript[k].CoinCollected)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (StarCoinPositions[i] == starCoinScript[k].PositionOfThisStarCoin)
                    {
                        StarCoinsInThisLevel[i] = false;
                    }
                }
            }
        }
    }
    */
}
