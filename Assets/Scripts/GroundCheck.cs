﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {


    private PlayerScript playerScript;
    
	
	void Start () 
    {
        playerScript = gameObject.GetComponentInParent<PlayerScript>();
	}
	
	
	void Update () 
    {
        //vaihdetaan GroundColliderin paikkaa jos painovoiman ja pelaajan asento vaihtuu
        if (!playerScript.gravityFlipped)
        {            
            transform.position = playerScript.transform.position +
                                    new Vector3(0f, -0.8f, 0f);
        }
        else
        {
            transform.position = playerScript.transform.position +
                                    new Vector3(0f, +0.8f, 0f);
        }
	}


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Platform")
        {
            playerScript.grounded = true;
        }
    }


    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Platform")
        {
            playerScript.grounded = true;
        }
    }


    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Platform")
        {
            playerScript.grounded = false;

            //nollataan napit kun ollaan ilmassa
            playerScript.jumpButtonPressed = false;
            playerScript.gravityButtonPressed = false;
        }
    }
}
