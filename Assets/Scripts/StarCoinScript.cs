﻿using UnityEngine;
using System.Collections;

public class StarCoinScript : MonoBehaviour {


    public Vector3 PositionOfThisStarCoin;

    
	void Start () 
    {
        
	}
	
	
	void Update () 
    {
	
	}


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            PositionOfThisStarCoin = new Vector3(   transform.position.x,
                                                    transform.position.y,
                                                    transform.position.z);

            //partikkelin spawnaus, tuhoamisen partikkeli hoitaa itse
            Instantiate(GameManagerScript.Instance.StarParticlePrefab,
                        PositionOfThisStarCoin, 
                        Quaternion.Euler(0, 0, 0));

            //tähden tuhoaminen
            Destroy(gameObject);
        }
    }

}
