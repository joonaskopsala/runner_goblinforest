﻿using UnityEngine;
using System.Collections;

public class DeathBorder : MonoBehaviour {


    private PlayerScript playerScript;


    void Start()
    {
        playerScript = GameObject.FindGameObjectWithTag("Player").
                       GetComponent<PlayerScript>();
    }

    //pelaaja kuolee kosketuksesta
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            playerScript.damageTaken = true;
        }
    }
}
