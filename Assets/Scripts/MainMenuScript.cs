﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {

	public string playGame;
	public string levelSelect;
	public string quitGame;
	public Text HighscoreText;
	public float highscore;

	// Use this for initialization
	void Start () {

		highscore = PlayerPrefs.GetFloat ("highscore");

	}
	
	// Update is called once per frame
	void Update () {

		HighscoreText.text = "Olet kerännyt yhteensä " + highscore + " kolikkoa.";

	}

	public void PlayGame(){
		Application.LoadLevel ("Level01");

	}

	public void LevelSelect(){
		Application.LoadLevel ("LevelSelect");
	}

	public void QuitGame(){
		Application.Quit ();
	}
}
