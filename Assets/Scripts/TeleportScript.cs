﻿using UnityEngine;
using System.Collections;

public class TeleportScript : MonoBehaviour {

    
    private TeleportOutScript teleportOutScript;
    private PlayerScript playerScript; 
    public CameraScript cameraScript;
    private Rigidbody2D playerRigid;

    public Vector3 teleportEnter;
    public Vector3 teleportExit;    

    private Vector2 vauhtiSailio;

    	
	private void Awake () 
    {
        playerScript = GameObject.FindGameObjectWithTag("Player").
                        GetComponent<PlayerScript>();
        playerRigid = GameObject.FindGameObjectWithTag("Player").
                        GetComponent<Rigidbody2D>();

        cameraScript = GameObject.FindGameObjectWithTag("MainCamera").
                        GetComponent<CameraScript>();

        teleportOutScript = GetComponentInChildren<TeleportOutScript>();

        //laitetan vauhtisäiliöön jotain
        vauhtiSailio = new Vector2(1f, 0f);
	}
	
	
	private void Update () {
	 //Camera.main.transform.position.x 
		if ((   playerScript.transform.position.x + 8f) == cameraScript.transform.position.x 
                && cameraScript.voimaBoolean) 
        {
			cameraScript.voimaBoolean = false;

			playerRigid.velocity = vauhtiSailio;
		}
	}

    private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player" && vauhtiSailio != new Vector2(0f, 0f))
		{
            //vauhti talteen
			vauhtiSailio = playerRigid.velocity;

			playerScript.telePorting = true;

            //kameran koordinaatit pelaaja scriptistä 
            teleportEnter = playerScript.deathPosition;

            //lisätään kameran tarvistema -10f
            teleportExit = teleportOutScript.transform.position;
			playerScript.transform.position = teleportExit;

			playerRigid.velocity = new Vector2(0f, 0f);
		}
	}
}
