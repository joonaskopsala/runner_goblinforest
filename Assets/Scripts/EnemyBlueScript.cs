﻿using UnityEngine;
using System.Collections;

public class EnemyBlueScript : MonoBehaviour {

    public PlayerScript playerScript;

	void Start () 
    {
        playerScript = GameObject.FindGameObjectWithTag("Player").
                       GetComponent<PlayerScript>();
	}
	
	void Update () 
    {
	
	}

    //pelaaja kuolee kosketuksesta
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            playerScript.damageTaken = true;
        }
    }
}
