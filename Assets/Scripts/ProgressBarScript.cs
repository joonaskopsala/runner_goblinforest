﻿using UnityEngine;
using System.Collections;

public class ProgressBarScript : MonoBehaviour {

	private GameObject maali;
	private GameObject player;
	private GameObject progressNappi;
	
	float progressValue;
	float progressPaikka;
	Vector3 aPos;
	float progressBarWidth;
	float startPos;

	void Awake()
	{
		player = GameObject.Find ("Player");
		maali = GameObject.Find ("Maali");
		progressNappi = GameObject.Find ("ProgressButton");

	}

	void Start()
	{
		progressBarWidth = (GameManagerScript.screenWidth)/3;
		aPos = progressNappi.transform.position;
		startPos = player.transform.position.x;
	}
	
	void FixedUpdate()
	{
		ProgressBarValue ();
	}


	public void ProgressBarValue()
	{
		progressValue = (player.transform.position.x - startPos) / (maali.transform.position.x - 1f - startPos);
		progressPaikka = (progressValue * (progressBarWidth));
		//print ("ppaikka " + progressPaikka);
		progressNappi.transform.position = aPos + new Vector3 (progressPaikka, 0f, 0f);
	}
}
