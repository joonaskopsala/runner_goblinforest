﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelSelectScript : MonoBehaviour {

	public string levelOne;
	public string levelTwo;
	public string mainMenu;
	public float coinsLvl1;
	public float coinsLvl2;

	public Text Lvl1Coins;
	public Text Lvl2Coins;


	// Use this for initialization
	void Start () {
	
		coinsLvl1 = PlayerPrefs.GetFloat ("coinsLvl1");
		coinsLvl2 = PlayerPrefs.GetFloat ("coinsLvl2");

	}
	
	// Update is called once per frame
	void Update () {
	
		Lvl1Coins.text = coinsLvl1.ToString();
		Lvl2Coins.text = coinsLvl2.ToString();


	}

	public void LevelOne(){
		Application.LoadLevel ("Level01");
	}

	public void LevelTwo(){
		Application.LoadLevel ("Level05");
	}

	public void QuitMenu(){
		Application.LoadLevel ("MainMenu");
	}
}
