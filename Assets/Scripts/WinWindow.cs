﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinWindow : MonoBehaviour {

	private uiManager uimanager;
	
	public GameObject winMenucanvas;

	private MaaliScript DMGscript;

	private PlayerScript playerScript;

	public Text KuolemaTeksti;

	public Text KolikkoTeksti;

	public float highscore;

	public float coinsLvl1;

	public float coinsLvl2;


	void Start(){

		uimanager = GameObject.Find ("UI_Canvas").GetComponent<uiManager> ();
		DMGscript = GameObject.Find ("Maali").GetComponent<MaaliScript> ();
		playerScript = GameObject.Find ("Player").GetComponent<PlayerScript> ();

		highscore = PlayerPrefs.GetFloat ("highscore");
	}

	// Update is called once per frame
	void Update () {	

	
		if (DMGscript.voittoMark) {

			winMenucanvas.SetActive (true);

			KolikkoTeksti.text = "Keräsit " + playerScript.coinsInThisScene + " kolikkoa.";

			KuolemaTeksti.text = "Kuolit " + playerScript.deathsInThisScene + " kertaa.";

		} 
		else {
			winMenucanvas.SetActive(false);
		}
	}

	public void Continue(){

		highscore = highscore + playerScript.coinsInThisScene;
		
		PlayerPrefs.SetFloat ("highscore", highscore);
		PlayerPrefs.Save ();

		if (Application.loadedLevel == 3) {
			Application.LoadLevel ("Level01");

			coinsLvl2 = playerScript.coinsInThisScene;
			PlayerPrefs.SetFloat ("coinsLvl2", coinsLvl2);
		} 

		else if (Application.loadedLevel == 4) {
			Application.LoadLevel ("Level05");

			coinsLvl1 = playerScript.coinsInThisScene;
			PlayerPrefs.SetFloat ("coinsLvl1", coinsLvl1);
		}


		Time.timeScale = 1;

		winMenucanvas.SetActive (false);
	}

	public void Select(){

		highscore = highscore + playerScript.coinsInThisScene;
		
		PlayerPrefs.SetFloat ("highscore", highscore);
		PlayerPrefs.Save ();

		Application.LoadLevel ("LevelSelect");

		if (Application.loadedLevel == 3) {
			
			coinsLvl2 = playerScript.coinsInThisScene;
			PlayerPrefs.SetFloat ("coinsLvl2", coinsLvl2);
		} 
		
		else if (Application.loadedLevel == 4) {

			coinsLvl1 = playerScript.coinsInThisScene;
			PlayerPrefs.SetFloat ("coinsLvl1", coinsLvl1);
		}

	}

	public void Quit(){

		if (Application.loadedLevel == 3) {
			
			coinsLvl2 = playerScript.coinsInThisScene;
			PlayerPrefs.SetFloat ("coinsLvl2", coinsLvl2);
		} 
		
		else if (Application.loadedLevel == 4) {
			
			coinsLvl1 = playerScript.coinsInThisScene;
			PlayerPrefs.SetFloat ("coinsLvl1", coinsLvl1);
		}

		highscore = highscore + playerScript.coinsInThisScene;
		
		PlayerPrefs.SetFloat ("highscore", highscore);
		PlayerPrefs.Save ();

		Application.LoadLevel("MainMenu");

	}
}
