﻿using UnityEngine;
using System.Collections;

public class LevelGoal : MonoBehaviour {


    public bool playerFinishedLevel = false;


    //nämä raahataan itse Inspectoriin
    public GameObject branch;
    public Transform from;
    public Transform to;

    public float startTime = 0f;
    public float percentToFinish = 0f;

	void Start () 
    {

	}
	
	
	void Update () 
    {
        if (playerFinishedLevel)
        {
            //lasketaan montako prosenttia lSlerppiä on jäljellä
            float speed = Time.time - startTime;
            percentToFinish = speed / 0.2f;

            branch.transform.rotation = Quaternion.Slerp(from.rotation,
                                        to.rotation,
                                        percentToFinish);            
        }       
	}
    

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            playerFinishedLevel = true;

            startTime = Time.time;
        }
    }
}
