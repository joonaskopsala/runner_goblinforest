﻿using UnityEngine;
using System.Collections;

public class BackgroundScript : MonoBehaviour
{

    public CameraScript cameraScript;

    public MidgroundFarScript midgroundFarScript;
    public MidgroundScript midgroundScript;
    public ForegroundScript foregroundScript;


    public Vector3 cameraPosition;
        
    //taustakuvan muuttuja
    public SpriteRenderer backgroundImage;

    public GameObject midground;


	void Start () 
    {
        cameraScript = GameObject.FindGameObjectWithTag("MainCamera").
                               GetComponent<CameraScript>();


        //ladataan Midground Resources-kansiosta
        midground = Resources.Load("Midground") as GameObject;
        
        //spawnataan alussa backgound jutut
        Instantiate(midground, new Vector3(5f, -11f, 11f), Quaternion.identity);
               
        //haetaan 
        midgroundScript = GameObject.FindGameObjectWithTag("Midground").
                        GetComponent<MidgroundScript>();

        print("Backgroundissa instantioitu: 5f, -11f, 11f");
        //GameObjectit talteen jotta ne voidaan spawnata
       // midground = GameObject.FindGameObjectWithTag("Midground");
        
	}
	
	
	void Update () 
    {
       // GetGameObjects();

        GetPositions();     
        
        
	}
    

    void GetGameObjects()
    {
        if (midground == null)
        {
            midground = GameObject.FindGameObjectWithTag("Midground");
        }
    }

    void GetPositions()
    {
        cameraPosition = cameraScript.transform.position;
    }

 
    public void SpawnMidground()
    {

        Instantiate(midground, new Vector3(5f, -11f, 11f), Quaternion.identity);
        print("Backgroundissa Spawn  5f, -11f, 11f");
    }

}
