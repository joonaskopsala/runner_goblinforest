﻿using UnityEngine;
using System.Collections;

public class MidgroundScript : MonoBehaviour {


    public CameraScript cameraScript;

    public BackgroundScript backgroundScript;


    void Start()
    {
        cameraScript = GameObject.FindGameObjectWithTag("MainCamera").
                        GetComponent<CameraScript>();

        backgroundScript = GameObject.FindGameObjectWithTag("Background").
                        GetComponent<BackgroundScript>();

        //laitetaan tämä MainCameran lapseksi
        this.transform.parent = GameObject.FindGameObjectWithTag("MainCamera").transform;

        transform.localPosition = new Vector3(0f, -11f, 11f);
    }


    void Update()
    {
        print("position: " + transform.position.x);
        print("localpos: " + transform.localPosition.x + " asdfasdfafdasdfasdfasdfasdfasdf");



        MoveMidground();

        DeleteMidground();
    }

    void MoveMidground()
    {
        transform.position = cameraScript.transform.position + new Vector3(
                    (cameraScript.transform.position.x * -0.2f), -11f, 11f);        
    }

    void DeleteMidground()
    {
        if (transform.localPosition.x <= -5f)
        {
            print("localpos.x: " + transform.localPosition.x );

            //tuhotaan tämä
            Destroy(gameObject);

            //luodaan uusi kun tämä on ulkona ruudusta
            backgroundScript.SpawnMidground();

            
        }
    }

}
