﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
    //näytön koko havaitaan automaattisesti
    public float screenHeight;
    public float screenWidth;

    //scenessä kerättävät asiat
    public int coinsInThisScene = 0;
    public int deathsInThisScene = 0;

    //pelaajan perus asetukset
    public float speed = 700f;
    public float jumpPower = 900f;

    public bool damageTaken = false;
    public bool playerMustWait = false;

    //pelaajan aloitus, kuolemis ja maalin positiot
    public Vector3 startPosition;
    public Vector3 endPosition;
    public Vector3 deathPosition;
    public Vector3 lastDeathPosition;

    //muuttujat pelaajan liikkumista varten
    public Rigidbody2D rb2D;
    public bool grounded = false;    
    public bool gravityFlipped = false;
    public bool jumpButtonPressed = false;
    public bool jumpHigh = false;
    public bool canJumpHigh = true;
    public float jumpStartTime;
    public bool gravityButtonPressed = false;
    public bool damageSquashed = false;

    public bool telePorting = false;

    //pelaaja on immuuni aloittaessa ja restartissa
    public bool playerImmune = true;
    
    //scenen aloitusta varten 
    public bool levelStarted = false;
    
    //pause ja tänne myös pause menu canvakset sun muut jos tarvii
    public bool paused = false;
    //jump ja gravity poissa käytöstä, jos esim. avataan valikko
    public bool disableMovementButtons = false;    

    //spawnattavat prefabit
  //  public GameObject EnemyBlueGroupPrefab;
  //  public GameObject EnemyBlueGroup;
  //  public GameObject CoinOneGroup;

    //kamera-ajoa varten
    public CameraScript cameraScript;
    //apumuuttuja kameralle kuolin animaation takia
    public bool cameraCanLerp = false;

    //jos pää osuu kattoon
    public UpperDamage upperDamageScript;

    //animaatiot täälä scriptissä
    public SpriteRotateScript spriteRotateScript;
    
    //LevelManager vihollisten spawn käskyä varten
    public LevelManagerScript levelManager;


    void Awake()
    {

        spriteRotateScript = GetComponentInChildren<SpriteRotateScript>();

        //jos pää kolahtaa mihin vain tulee damagea
        upperDamageScript = GetComponentInChildren<UpperDamage>();

        //rigidbody pelaajan liikutusta varten
        rb2D = gameObject.GetComponent<Rigidbody2D>();

        //kamera-ajoa varten haetaan cameraScript
        cameraScript = GameObject.FindGameObjectWithTag("MainCamera").
                        GetComponent<CameraScript>();        
    }


    void Start()
    {        
        //otetaan aloitus positio talteen
        startPosition = transform.position;
        
        //näytön koko talteen
        screenHeight = GameManagerScript.screenHeight;
        screenWidth = GameManagerScript.screenWidth;

        //LevelManagerissa luodaan ja tuhotaan viholliset
        levelManager = GameObject.FindGameObjectWithTag("LevelManager").
                        GetComponent<LevelManagerScript>();
    }


    void Update()
    {
        //kerätään tietoja heti Updaten alussa
        CollectStatistics();        

		GetButtons();

        StartLevel();
        
        DamageTakenAnimation();

        DamageIfVelocityTooSmall();
        
        DeathAndRestart();

        SavePlayerStats();

    }

    
    void FixedUpdate()
    {
        //pelaajaan vaikuttavien voimien muokkaaminen FixedUpdatessa
        MovePlayer();	
    }    
    
    void SavePlayerStats()
    {
        //Player.prefs? vai mikä onkaan kerää tiedot talteen kun level on pelattu läpi
    }
    
    void CollectStatistics()
    {
        //kuolemiskoordinaatit talteen jokavuoro, koska tätä ei kerkeä tekemään kuoleman sattuessa
        //koordinaatit kamerasta, ei pelaajasta koska tätä käytetään CameraScriptissä
        deathPosition = cameraScript.transform.position;
    }
    
    void DamageTakenAnimation()
    {
        if (damageTaken && !playerMustWait)
        {
            playerMustWait = true;
            //pysäytetään pelaaja
            rb2D.velocity = new Vector2(0f, 0f);   
            
            //käännetään painovoima alaspäin jotta pelaaja tippuu
            if (gravityFlipped)
            {                
                rb2D.gravityScale *= -1f;
            }

            StartCoroutine(WaitAfterDeathAnimation());
        }
    }
    //parin sekunnin viive TÄMÄN paikalle sopii myös restart nappula
    public IEnumerator WaitAfterDeathAnimation()
    {
        float timeToWait = 1f;

        yield return new WaitForSeconds(timeToWait);

        cameraCanLerp = true;
        playerMustWait = false;

        yield return new WaitForSeconds(timeToWait); 
    }


    void DeathAndRestart()
    {
        //kamera-ajo (cameraIsLerping) radan alkuun CameraScriptissä 
        if (damageTaken && playerMustWait && cameraScript.cameraIsLerping)
        {
            //otetaan kuolema koordinaatit talteen
            lastDeathPosition = transform.position;

            //alkuasetukset kuoleman jälkeen
            damageSquashed = false;
            playerMustWait = false;
            levelStarted = false;
            damageTaken = false;
            deathsInThisScene++;
            rb2D.velocity = new Vector2(0f, 0f);

            //siirretään pelaaja radan alkuun
            transform.position = startPosition;
            if (gravityFlipped)
            {
                gravityFlipped = false;
                rb2D.gravityScale *= -1f;
                transform.localScale = new Vector3(1, 1, 1);
            }            
            
            //tuhotaan viholliset ja luodaan ne uudestaan täältä
            levelManager.spawnEnemies();            
        }        
    }


    void StartLevel()
    {
        //pidetään peli pysähdyksissä heti alussa ennenkuin nappeja on painettu
        if (!levelStarted && cameraScript.cameraIsLerping)
        {
            Time.timeScale = 1f;


            
        }
    //katsotaan pelaataanko puhelimella ja määrätään kumpia nappeja käytetään
    #if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
        //aloitetaan peli kun painetaan jotain nappia. tänne myös musiikin soitan aloittaminen
        if((( Input.GetKeyDown(KeyCode.S) 
           || Input.GetKeyDown(KeyCode.A)) 
           && !levelStarted) 
           && !cameraScript.cameraIsLerping)
        {
            jumpButtonPressed = false;
            canJumpHigh = false;
            gravityButtonPressed = false;
            levelStarted = true;
            cameraCanLerp = false;
            Time.timeScale = 1f;

            //työnnetään pelaaja liikkeelle joka kestää koko pelin ajan
            rb2D.AddForce(Vector2.right * speed);

            //laitetaan pelaaja immuuniksi puoleksi sekunniksi että vauhti kerkeää kiihtyä
            playerImmune = true;            
            StartCoroutine(WaitPlayerNotToBeImmune());
        }
    #else
        //tutkitaan onko painettu ja aloitetaan peli
        int nbTouches = Input.touchCount;

        if ((nbTouches > 0 && !levelStarted) 
            && !cameraScript.cameraIsLerping 
            && !disableMovementButtons)
        {
            jumpButtonPressed = false;
            canJumpHigh = false;
            gravityButtonPressed = false;
            levelStarted = true;
            cameraCanLerp = false;
            Time.timeScale = 1f;
            
            rb2D.velocity = new Vector2(0, 0);
            rb2D.AddForce(Vector2.right * speed);

            //laitetaan pelaaja immuuniksi puoleksi sekunniksi että vauhti kerkeää kiihtyä
            playerImmune = true;            
            StartCoroutine(WaitPlayerNotToBeImmune());
        }
#endif
    }


    void GetButtons()
    {
    //katsotaan pelaataanko puhelimella ja määrätään kumpia nappeja käytetään
    #if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
                
        //perushyppy
        if (Input.GetKeyDown(KeyCode.A) && grounded && levelStarted && !damageTaken)
        {
            jumpButtonPressed = true;
            upperDamageScript.gravityButtonPressed_2 = true;

            spriteRotateScript.JumpPressedForAnim = true;

            jumpStartTime = Time.time;

            canJumpHigh = true;
        }

        //jos hyppy napin päästä kesken ilmalennon ei voi enää hypätä korkeammalle
        if (Input.GetKeyUp(KeyCode.A) && levelStarted)
        {
            canJumpHigh = false;
        }

        //kun nappia painetaan pohjassa yli 0.4s tehdään korkeampi hyppy
        if (Input.GetKey(KeyCode.A) && canJumpHigh && levelStarted &&
            Time.time > (jumpStartTime + 0.4f))
        {
            jumpHigh = true;

            //animaatiota varten säädetään boolean täältä, 
            //nollaus spriteRotateScriptissä
            spriteRotateScript.JumpHighForAnim = true;
        }
        
        //painovoiman vaihto
        if (Input.GetKeyDown(KeyCode.S) && grounded && levelStarted && !damageTaken)
        {
            gravityButtonPressed = true;
            upperDamageScript.gravityButtonPressed_2 = true;
        }

    #else
        //tutkitaan kaikki kosketukset ja niiden sijainti
        int nbTouches = Input.touchCount;

        if (nbTouches > 0)
        {
            for( int i = 0; i < nbTouches; i++)
            {
                Touch touch = Input.GetTouch(i);
                
                //perushyppy
                //tarkastetaan osuiko painallus vasempaan laitaan ja 4/5 osaan alalaitaa
                if (touch.phase ==  TouchPhase.Began 
                                    && grounded
                                    && levelStarted
                                    && !jumpButtonPressed                                         
                                    && !disableMovementButtons
                                    && !damageTaken
                                    && (touch.position.x < (screenWidth * 0.5f)
                                    && (touch.position.y < (screenHeight * 0.8f))))
                {
                    jumpButtonPressed = true;  
                    upperDamageScript.gravityButtonPressed_2 = true;
                    spriteRotateScript.JumpPressedForAnim = true;

                    jumpStartTime = Time.time;                    
                    canJumpHigh = true;
        
                }

                //jos hyppy napin päästä kesken ilmalennon ei voi enää hypätä korkeammalle
                if( touch.phase ==  TouchPhase.Ended
                                    && levelStarted
                                    && !disableMovementButtons
                                    && (touch.position.x < (screenWidth * 0.5f)
                                    && (touch.position.y < (screenHeight * 0.8f))))
                {
                    canJumpHigh = false;
                }
        
                //kun nappia painetaan pohjassa yli 0.4s tehdään korkeampi hyppy
                if (touch.phase ==  TouchPhase.Stationary 
                                    && levelStarted
                                    && !disableMovementButtons
                                    && canJumpHigh
                                    && Time.time > (jumpStartTime + 0.4f)
                                    && (touch.position.x < (screenWidth * 0.5f)
                                    && (touch.position.y < (screenHeight * 0.8f))))
                {
                    jumpHigh = true;

                    //animaatiota varten säädetään boolean täältä, 
                    //nollaus spriteRotateScriptissä
                    spriteRotateScript.JumpHighForAnim = true;
                }



                //tarkastetaan osuiko painallus oikeaan laitaan ja 4/5 osaan alalaitaa
                if (touch.phase ==  TouchPhase.Began 
                                    && grounded 
                                    && levelStarted
                                    && !gravityButtonPressed                                         
                                    && !disableMovementButtons
                                    && !damageTaken
                                    && (touch.position.x > (screenWidth * 0.5f) 
                                    && (touch.position.y < (screenHeight * 0.8f))))
                {
                    gravityButtonPressed = true;
                    upperDamageScript.gravityButtonPressed_2 = true;
                }
                
                // 1/5 osa ylälaidasta on pause
                if (touch.phase == TouchPhase.Began &&
                    touch.position.y > (screenHeight * 0.8f))
                {
                    paused = !paused;
                }
            }
        }
#endif
    }

    void MovePlayer()
    {
        if (jumpButtonPressed)
        {
            if (!gravityFlipped)
            {
                rb2D.AddForce(Vector2.up * jumpPower);
            }

            if (gravityFlipped)
            {
                rb2D.AddForce(Vector2.up * jumpPower * -1f);
            }

            jumpButtonPressed = false;
            spriteRotateScript.JumpPressedForAnim = false;
        }

        if (gravityButtonPressed)
        {
            //painovoiman vaihto
            rb2D.gravityScale *= -1f;

            //hahmon flippaus
            transform.localScale = new Vector3(1, (transform.localScale.y * -1f), 1);

            if (gravityFlipped)
            {
                gravityFlipped = false;
            }
            else
            {
                gravityFlipped = true;
            }

            gravityButtonPressed = false;
        }

        //korkeampi hyppy
        if (jumpHigh)
        {
            if (!gravityFlipped)
            {
                rb2D.AddForce(Vector2.up * jumpPower * 0.9f);
            }
            else
            {
                rb2D.AddForce(Vector2.up * jumpPower * -0.9f);
            }
            
            canJumpHigh = false;
            jumpHigh = false;
        }
    }

    
    void DamageIfVelocityTooSmall()
    {
        //pelaaja on kuolematon aivan radan ja restartin alussa
        if (!playerImmune)
        {
            //jos vauhti hidastuu pelaaja ottaa vahinkoa
            if ((rb2D.velocity.x < 13.9f || rb2D.velocity.x > 14.1f) && levelStarted)
            {
                damageTaken = true;
            }
        }        
    }

    public IEnumerator WaitPlayerNotToBeImmune()
    {
        float timeToWait =0.5f;

        yield return new WaitForSeconds(timeToWait);

        playerImmune = false;
    }
}