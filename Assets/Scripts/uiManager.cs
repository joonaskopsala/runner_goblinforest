﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class uiManager : MonoBehaviour {

	public bool paused;

	private PlayerScript playerScript;
	private CountDownScript countdownScript;

	public Text DeathText;
	public Text CoinText;
	public Text LevelNumber;

	public string countdown = "";
	public bool showCountdown = false;
	public Text countDownText;


	// Use this for initialization
	void Start () {

		paused = false;

		if (Time.timeScale == 0f) {
			Time.timeScale = 1f;
		}

		playerScript = GameObject.FindGameObjectWithTag("Player").
			GetComponent<PlayerScript>();
		countdownScript = GameObject.Find ("UI_Canvas").GetComponent<CountDownScript> ();

		//deathCounter = 0;
		
		DeathText.text = "Deaths: ";
		CoinText.text = "Coins: ";

	}

	// Update is called once per frame
	void Update () {
		DeathCounter ();
		CoinCounter ();
		LevelNumberCheck ();
		countDownCheck ();
	}

	public void Pause(){

		paused = !paused;
	
		if (paused) {
			Time.timeScale = 0f;
            playerScript.disableMovementButtons = true;
            playerScript.jumpButtonPressed = false;
            playerScript.gravityButtonPressed = false;
		} 

		else if (!paused) {
		   // Time.timeScale = 1f;
			StartCoroutine(getReady());
            playerScript.disableMovementButtons = false;
            playerScript.jumpButtonPressed = false;
            playerScript.gravityButtonPressed = false;
		}
	}


    public void DeathCounter(){

		DeathText.text = "Deaths: " + playerScript.deathsInThisScene;

	}

	public void CoinCounter(){

		CoinText.text = "Coins: " + playerScript.coinsInThisScene;

	}

	public void LevelNumberCheck(){

		if (Application.loadedLevel == 4) {
			LevelNumber.text = "Lvl: 1";
		}
		else if (Application.loadedLevel == 3) {
			LevelNumber.text = "Lvl: 2";
		}
	//	LevelNumber.text = "Lvl: " + Application.loadedLevel;

	}

	public IEnumerator getReady()    
	{
		print ("getready active");
		showCountdown = true;    
		
		countdown = "3";    
		yield return StartCoroutine (WaitForRealSeconds(0.5f));
		
		countdown = "2";    
		yield return StartCoroutine (WaitForRealSeconds(0.5f));
		
		countdown = "1";    
		yield return StartCoroutine (WaitForRealSeconds(0.5f));
		
		countdown = "GO";    
		yield return StartCoroutine (WaitForRealSeconds(0.5f));
		
		countdown = "";
		showCountdown = false;
		
		Time.timeScale = 1;
		countDownText.text = "";
	}
	
	IEnumerator WaitForRealSeconds (float waitTime) 
	{
		float endTime = Time.realtimeSinceStartup+waitTime;
		
		while (Time.realtimeSinceStartup < endTime) 
		{
			yield return null;
		}
	}

	public void countDownCheck(){
		
		if (showCountdown) {
			countDownText.text = countdown;
		}
	}

    public void Testi()
    {
        print("testi");
    }
}
