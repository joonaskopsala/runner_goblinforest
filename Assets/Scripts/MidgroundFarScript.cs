﻿using UnityEngine;
using System.Collections;

public class MidgroundFarScript : MonoBehaviour {


    public CameraScript cameraScript;



    void Start()
    {
        cameraScript = GameObject.FindGameObjectWithTag("MainCamera").
                        GetComponent<CameraScript>();

        //laitetaan tämä MainCameran lapseksi
        this.transform.parent = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }


    void Update()
    {
        MoveMidgroundFar();

        DeleteMidgroundFar();
    }

    void MoveMidgroundFar()
    {
        transform.position = cameraScript.transform.position + new Vector3(
                    (cameraScript.transform.position.x * -0.1f), -4f, 11f);
    }

    void DeleteMidgroundFar()
    {
        if (transform.localPosition.x <= -20f)
        {
            Destroy(this);
        }
    }
}
