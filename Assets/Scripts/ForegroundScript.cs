﻿using UnityEngine;
using System.Collections;

public class ForegroundScript : MonoBehaviour {


    public CameraScript cameraScript;

    public Vector3 foregroundPosition;



	void Start () 
    {
        cameraScript = GameObject.FindGameObjectWithTag("MainCamera").
                        GetComponent<CameraScript>();	
	}
	
	
	void Update () 
    {
        foregroundPosition = transform.position;
	    
	}
}
