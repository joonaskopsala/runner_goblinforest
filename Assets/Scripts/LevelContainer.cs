﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


[XmlRoot("LevelCollection")]
public class LevelContainer
{
    [XmlArray("Levels")]
    [XmlArrayItem("Level")]
    public List<Level> Levels = new List<Level>();
    //public Level[] Levels;

    //loads xml directly from the given string. useful in combination with www.text
    public static LevelContainer LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(LevelContainer));
        return serializer.Deserialize(new StringReader(text)) as LevelContainer;
    }

    //load, tätä käytetään
    public static LevelContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(LevelContainer));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as LevelContainer;
        }
    }

    //save
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(LevelContainer));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
}