﻿using UnityEngine;
using System.Collections;

public class Level05Script : MonoBehaviour {


    //taustakuvan muuttuja
    public SpriteRenderer backgroundImage;
    //aseta haluttu taustakuva inspectorissa
    public Sprite newImage;
    
    //tämä määrää mitkä tähtikolikot on jo kerätty levelistä
    public bool[] StarCoinsInThisLevel = new bool[3];
    //starcoinien positionit levelissä
    public Vector3[] StarCoinPositions = new Vector3[3];
    //tähän taulukkoon tähtikolikot
    public GameObject[] StarCoins = new GameObject[3];

    //maaliScript maaliviivan (voittoMark) ohittamista varten
    public MaaliScript maaliScript;
    public bool kerranVain = true;
        

    
	void Start () 
    {
        maaliScript = GameObject.FindGameObjectWithTag("Maali").GetComponent<MaaliScript>();

        GetUncollectedStarCoins();
        SetStarCoinPositions();
        SpawnStarCoins();

        //haetaan backgroundin SpriteRenderer
        backgroundImage = GameObject.FindGameObjectWithTag("Background").
                            GetComponent<SpriteRenderer>();
        //asetetaan inspectorissa määritelty kuva taustaksi
        backgroundImage.sprite = newImage;
	}


    void Update() 
    {
        //tämä tarkastus pitää tehdä radan lopussa eikä joka framella
        if (maaliScript.voittoMark && kerranVain)
        {            
            CheckCollectedCoins();
            kerranVain = false;
        }        
	}


    void CheckCollectedCoins()
    {
        //käydään läpi kaikkien kolmen kolikon sijainnit
        for (int i = 0; i < 3; i++)
        {
            RaycastHit2D hit = Physics2D.Raycast(StarCoinPositions[i], Vector2.up, 0.1f);

            //jos sijainnissa ei ole mitään kolikko on kerätty
            if (hit.collider == null)
            {
                StarCoinsInThisLevel[i] = false;                
            }
        }        
    }


    void GetUncollectedStarCoins()
    {
        //haetaan tähtikolikkojen määrä leveliin jos joitain on jo kerätty

        //tämä for vain testi käyttöä varten, oikeasti kolikot haetaan XML:stä        
        StarCoinsInThisLevel[0] = true;
        StarCoinsInThisLevel[1] = true;
        StarCoinsInThisLevel[2] = true;
    }


    void SetStarCoinPositions()
    {
        //KIRJOITAITSE! haetaan XML:stä
        //kolikkojen positionit scenessä kirjoitetaan itse tähän
        StarCoinPositions[0] = new Vector3(10f, 0f, 0f);
        StarCoinPositions[1] = new Vector3(130f, -3f, 0f);
        StarCoinPositions[2] = new Vector3(200f, 2.8f, 0f);
    }


    void SpawnStarCoins()
    {
        //luodaan tarvittavat tähtikolikot
        for (int i = 0; i < 3; i++)
        {
            if (StarCoinsInThisLevel[i])
            {
                StarCoins[i] = Instantiate( GameManagerScript.Instance.StarCoinPrefab,
                                            StarCoinPositions[i],
                                            Quaternion.Euler(0, 0, 0)) as GameObject;
            }
        }
    }
}
