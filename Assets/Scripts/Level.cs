﻿using System.Xml;
using System.Xml.Serialization;


public class Level
{
    [XmlAttribute("level")]

    //levelin tiedosto nimi jolla se ladataan: Level01, Level05
    public string LevelName; 

    //onko leveliä mahdollista pelata
    public bool LevelLocked;

    //onko ensimmäistä star coinia kerätty 
    public bool StarCoin1NotCollected;
    //star coin1:n koordinaatit taulukossa
    public float[] StarCoin1Pos = new float[3];

    //onko toista star coinia kerätty 
    public bool StarCoin2NotCollected;
    //star coin1:n koordinaatit taulukossa
    public float[] StarCoin2Pos = new float[3];

    //onko kolmatta star coinia kerätty 
    public bool StarCoin3NotCollected;
    //star coin1:n koordinaatit taulukossa
    public float[] StarCoin3Pos = new float[3];
}