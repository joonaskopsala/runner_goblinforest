﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;

public class LevelManagerScript : MonoBehaviour {

    //JOS TÄMÄ ON TRUE KAIKKI KOLIKOT SPAWNATAAN UUDESTAAN JOKA KÄYNNISTYKSELLÄ, joka on kätevä testauksessa    
    public bool dontSaveXml = true;

    //PlayerScript damagen huomaamista varten, jolla spawnataan viholliset uudestaan leveliin
    public PlayerScript playerScript;

    //taustakuvan muuttuja
    public SpriteRenderer backgroundImage;
    //aseta haluttu taustakuva inspectorissa
    public Sprite newImage;

    //tämä määrää mitkä tähtikolikot on jo kerätty levelistä
    public bool[] StarCoinsInThisLevel = new bool[3];
    //starcoinien positionit levelissä
    public Vector3[] StarCoinPositions = new Vector3[3];
    //tähän taulukkoon tähtikolikot
    public GameObject[] StarCoins = new GameObject[3];

    //maaliScript maaliviivan (voittoMark) ohittamista varten
    public MaaliScript maaliScript;
    public bool kerranVain = true;
    
    //kaikki viholliset samassa Gameobjectissa
    public GameObject allEnemiesStorage;
    //apumuuttuja vihollisille jota tuhotaan ja instantioidaan
    public GameObject allEnemies;

    //apuboolean ettei vihollisia spawnata montaa sekuntia kun pelaaja kuolee
    public bool canSpawnEnemies = true;
            
    //scenen nimi, esim. Level01, Level02
    public string loadedLevel;

    //xml tiedostosta leveliä koskeva tieto tämän sisälle
    public LevelContainer levelCollection;

        
    void Start()
    {
        maaliScript = GameObject.FindGameObjectWithTag("Maali").GetComponent<MaaliScript>();
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();

        //ladatunscenen nimi talteen
        loadedLevel = Application.loadedLevelName;

        //ladataan xml levelCollectioniin
        LoadXml();
        
        //tarkastetaan mitkä kolikot on kerätty
        GetUncollectedStarCoins();

        //haetaan kolikkojen koordinaatit
        GetStarCoinCoordinates();
                
        //luodaan kolikot
        SpawnStarCoins();

        //EI KÄYTÖSSÄ, jos tämä toimii backgroundista, niin poista
        //spriten vaihto Backgounr gameobjectiin
        //SetBackground();

        //vihollisten lataus Resources-hakemistosta, ku muuten ei voi automaattisesti
        allEnemiesStorage = Resources.Load("AllEnemiesGroup_" + loadedLevel) as GameObject;
        //allEnemiesStorage = Resources.Load("EnemyBlueGroup_" + loadedLevel) as GameObject;
        allEnemies = Instantiate(allEnemiesStorage);        
    }    

    void Update()
    {        
        //tämä tarkastus pitää tehdä radan lopussa eikä joka framella
        if (maaliScript.voittoMark && kerranVain)
        {
            CheckCollectedCoins();

            SaveToXml();

            kerranVain = false;

            //tuhotaan kaikki viholliset ja luodaan heti uudestaan
            Destroy(allEnemies);
            allEnemies = Instantiate(allEnemiesStorage);                        
        }
    }

    //jos pelaaja kuolee luodaan viholliset uudelleen, PlayerScripti käyttää tätä
    public void spawnEnemies()
    {
        //tuhotaan kaikki viholliset ja luodaan heti uudestaan
        Destroy(allEnemies);
        allEnemies = Instantiate(allEnemiesStorage);
        print("Destroy(allEnemies) " + "JA" + " Instantiate(allEnemiesStorage)");
    }

    void LoadXml()
    {
        //ladataan xml levelCollectioniin
        levelCollection = LevelContainer.Load(Path.Combine(Application.persistentDataPath, "levels.xml"));
        print("Copy paste tämä jos pitää muokata levels.xml: " + Application.persistentDataPath);
    }


    void GetStarCoinCoordinates()
    {
        //käydään läpi levelCollectionia ja etsitään levelin nimeä
        foreach (Level level in levelCollection.Levels)
        {
            //kun oikea scene sattuu kohdalle
            if( level.LevelName.Equals(loadedLevel))
            {
                if (level.StarCoin1NotCollected)
                {
                    float starCoinXPos = level.StarCoin1Pos[0];
                    
                    float starCoinYPos = level.StarCoin1Pos[1];
                    
                    float starCoinZPos = level.StarCoin1Pos[2];
                    
                    StarCoinPositions[0] = new Vector3( starCoinXPos,
                                                        starCoinYPos,
                                                        starCoinZPos);
                }

                if (level.StarCoin2NotCollected)
                {
                    float starCoinXPos = level.StarCoin2Pos[0];

                    float starCoinYPos = level.StarCoin2Pos[1];

                    float starCoinZPos = level.StarCoin2Pos[2];

                    StarCoinPositions[1] = new Vector3( starCoinXPos,
                                                        starCoinYPos,
                                                        starCoinZPos);
                }

                if (level.StarCoin3NotCollected)
                {
                    float starCoinXPos = level.StarCoin3Pos[0];

                    float starCoinYPos = level.StarCoin3Pos[1];

                    float starCoinZPos = level.StarCoin3Pos[2];

                    StarCoinPositions[2] = new Vector3(starCoinXPos,
                                                        starCoinYPos,
                                                        starCoinZPos);
                }
            }
        }
    }
    
    void LoadAndSaveXml()
    {
        //ladataan xml levelCollectioniin
        levelCollection = LevelContainer.Load(Path.Combine(Application.persistentDataPath, "levels.xml"));

        //käydään läpi levelCollectionia ja etsitään levelin nimeä
        foreach( Level level in levelCollection.Levels)
        {
            if (level.LevelName.Equals("Level05"))
            {
                //muutetaan arvoa
                level.LevelLocked = true;
                
                //savetetaan xml
                levelCollection.Save(Path.Combine(Application.persistentDataPath, "levels.xml"));
            }
        }
    }
        
    void GetUncollectedStarCoins()
    {
        //laitetaan kaikki trueksi, niin ei tartte elsejä iffien jälkeen laittamaan trueksi
        StarCoinsInThisLevel[0] = true;
        StarCoinsInThisLevel[1] = true;
        StarCoinsInThisLevel[2] = true;

        foreach (Level level in levelCollection.Levels)
        {
            //kun oikea scene sattuu kohdalle
            if (level.LevelName.Equals(loadedLevel))
            {
                if (!level.StarCoin1NotCollected)
                {
                    StarCoinsInThisLevel[0] = false;
                }

                if (!level.StarCoin2NotCollected)
                {
                    StarCoinsInThisLevel[1] = false;
                }

                if (!level.StarCoin3NotCollected)
                {
                    StarCoinsInThisLevel[2] = false;
                }
            }
        }
    }

    void SpawnStarCoins()
    {
        //luodaan tarvittavat tähtikolikot
        for (int i = 0; i < 3; i++)
        {
            if (StarCoinsInThisLevel[i])
            {
                StarCoins[i] = Instantiate(GameManagerScript.Instance.StarCoinPrefab,
                                            StarCoinPositions[i],
                                            Quaternion.Euler(0, 0, 0)) as GameObject;
            }
        }
    }

    void CheckCollectedCoins()
    {
        //käydään läpi kaikkien kolmen kolikon sijainnit Raycastilla ja tarkastetaan onko kolikkoa
        for (int i = 0; i < 3; i++)
        {
            RaycastHit2D hit = Physics2D.Raycast(StarCoinPositions[i], Vector2.up, 0.1f);

            //jos sijainnissa ei ole mitään kolikko on kerätty
            if (hit.collider == null)
            {
                StarCoinsInThisLevel[i] = false;
            }
        }
    }

    void SaveToXml()
    {
        //jos true XML ei kirjoiteta päälle
        if (!dontSaveXml)
        {
            foreach (Level level in levelCollection.Levels)
            {
                //kun oikea scene sattuu kohdalle
                if (level.LevelName.Equals(loadedLevel))
                {
                    //päivitetään kerätyt starcoinit xml:ään
                    level.StarCoin1NotCollected = StarCoinsInThisLevel[0];
                    level.StarCoin2NotCollected = StarCoinsInThisLevel[1];
                    level.StarCoin3NotCollected = StarCoinsInThisLevel[2];

                    //savetetaan xml
                    levelCollection.Save(Path.Combine(Application.persistentDataPath, "levels.xml"));
                }
            }
        }
    }
    
    //EI KÄYTÖSSÄ, jos tämän voi hoitaa BackgroundScriptissä, niin poista
    void SetBackground()
    {
        //haetaan backgroundin SpriteRenderer
        backgroundImage = GameObject.FindGameObjectWithTag("Background").
                            GetComponent<SpriteRenderer>();
        //asetetaan inspectorissa määritelty kuva taustaksi
        backgroundImage.sprite = newImage;
    }
}