﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountDownScript : MonoBehaviour {


	public string countdown = "";

	public bool showCountdown = false;

	private Text countDownText;

	void Start(){

		countDownText = GameObject.Find ("CountDownText").GetComponent <Text>();

	}

	void Update(){

		countDownCheck ();

	}

	public IEnumerator getReady()    
	{
		print ("getready active");
		showCountdown = true;    
		
		countdown = "3";    
		yield return StartCoroutine (WaitForRealSeconds(0.5f));
		
		countdown = "2";    
		yield return StartCoroutine (WaitForRealSeconds(0.5f));
		
		countdown = "1";    
		yield return StartCoroutine (WaitForRealSeconds(0.5f));
		
		countdown = "GO";    
		yield return StartCoroutine (WaitForRealSeconds(0.5f));
		
		countdown = "";
		showCountdown = false;
		
		Time.timeScale = 1;
	}

	IEnumerator WaitForRealSeconds (float waitTime) 
	{
		float endTime = Time.realtimeSinceStartup+waitTime;
		
		while (Time.realtimeSinceStartup < endTime) 
		{
			yield return null;
		}
	}

	public void countDownCheck(){
		
		if (showCountdown) {
			countDownText.text = countdown;
		}
	}
}
